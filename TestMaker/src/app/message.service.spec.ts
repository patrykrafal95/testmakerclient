import { TestBed } from '@angular/core/testing';
import { ToastrService } from 'ngx-toastr';
import { MessageService } from './message.service';
describe('MessageService', () => {
  let service: MessageService;
  beforeEach(() => {
    const toastrServiceStub = { success: () => ({}), error: () => ({}) };
    TestBed.configureTestingModule({
      providers: [
        MessageService,
        { provide: ToastrService, useValue: toastrServiceStub }
      ]
    });
    service = TestBed.get(MessageService);
  });
  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});

import { CommonModule } from '@angular/common';
import { ResultEditComponent } from './../result/result-edit/result-edit.component';
import { ResultListComponent } from './../result/result-list/result-list.component';
import { QuestionEditComponent } from './../question/question-edit/question-edit.component';
import { AnswerEditComponent } from './../answer/answer-edit/answer-edit.component';
import { AnswerListComponent } from './../answer/answer-list/answer-list.component';
import { QuestionListComponent } from './../question/question-list/question-list.component';
import { NgModule } from '@angular/core';
import { LoadingComponent } from './loading/loading.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { ConfirmDeleteResultComponent } from '../result/confirm-delete-result/confirm-delete-result.component';
import { RouterModule } from '@angular/router';
import { ShowMoreLesTextDirective } from './directives/show-more-les-text.directive';
import { ShowMoreLessElementsDirective } from './directives/show-more-less-elements.directive';
import { LimitCharacterDirective } from './directives/limit-character.directive';
import { DetailsInformationTableDirective } from './directives/details-information-table.directive';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    LoadingComponent,
    ConfirmDeleteComponent,
    QuestionListComponent,
    QuestionEditComponent,
    AnswerListComponent,
    AnswerEditComponent,
    ResultListComponent,
    ResultEditComponent,
    ConfirmDeleteResultComponent,
    ShowMoreLesTextDirective,
    ShowMoreLessElementsDirective,
    LimitCharacterDirective,
    DetailsInformationTableDirective
  ],
  exports: [
    FormsModule,
    CommonModule,
    LoadingComponent,
    AnswerListComponent,
    ConfirmDeleteComponent,
    QuestionListComponent,
    ResultListComponent,
    ReactiveFormsModule,
    RouterModule,
    ShowMoreLesTextDirective,
    ShowMoreLessElementsDirective,
    DetailsInformationTableDirective
  ]
})
export class SharedModule { }

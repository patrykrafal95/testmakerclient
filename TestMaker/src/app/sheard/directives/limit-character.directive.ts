import { Directive, OnInit, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[shrLimitCharacter]'
})
export class LimitCharacterDirective implements OnInit {


  private _element: ElementRef;

  constructor(private _er: ElementRef) {
    this._element = _er.nativeElement;
  }

  ngOnInit() {
    const pattern = /\D/g;
    $(this._element).off('keyup').on('keyup', function (e) {
      e.preventDefault();

      if (pattern.test(this.value)) {
        this.value = this.value.replace(pattern, '');
      }
    });
  }


}

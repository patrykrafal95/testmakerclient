import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement} from '@angular/core';
import { By } from '@angular/platform-browser';
import { ElementRef } from '@angular/core';
import { ShowMoreLessElementsDirective } from './show-more-less-elements.directive';

@Component({
  template: `
    <div>Without Directive</div>
    <div shrShowMoreLessElements>Default</div>
  `
})
class TestComponent {}

describe('ShowMoreLessElementsDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let elementsWithDirective: Array<DebugElement>;
  let bareElement: DebugElement;
  beforeEach(() => {
    const elementRefStub = { nativeElement: {} };
    const simpleChangesStub = {};
    TestBed.configureTestingModule({
      declarations: [ShowMoreLessElementsDirective, TestComponent]
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    elementsWithDirective = fixture.debugElement.queryAll(
      By.directive(ShowMoreLessElementsDirective)
    );
    bareElement = fixture.debugElement.query(
      By.css(':not([shrShowMoreLessElements])')
    );
  });
  it('should have bare element', () => {
    expect(bareElement).toBeTruthy();
  });

});

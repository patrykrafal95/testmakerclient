import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ElementRef } from '@angular/core';
import { SimpleChanges } from '@angular/core';
import { ShowMoreLesTextDirective } from './show-more-les-text.directive';

@Component({
  template: `
    <div>Without Directive</div>
    <div shrShowMoreLesText>Default</div>
  `
})
class TestComponent {}

describe('ShowMoreLesTextDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let elementsWithDirective: Array<DebugElement>;
  let bareElement: DebugElement;
  beforeEach(() => {
    const elementRefStub = { nativeElement: {} };
    const simpleChangesStub = {};
    TestBed.configureTestingModule({
      declarations: [ShowMoreLesTextDirective, TestComponent]
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    elementsWithDirective = fixture.debugElement.queryAll(
      By.directive(ShowMoreLesTextDirective)
    );
    bareElement = fixture.debugElement.query(
      By.css(':not([shrShowMoreLesText])')
    );
  });
});

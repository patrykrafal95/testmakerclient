import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ElementRef } from '@angular/core';
import { LimitCharacterDirective } from './limit-character.directive';



describe('LimitCharacterDirective', () => {
  const pattern = /\D/g;
  beforeEach(() => {
    const elementRefStub = { nativeElement: {} };
    TestBed.configureTestingModule({
      declarations: [LimitCharacterDirective]
    });

  });
  it('test regular expression find only letter', () => {
    expect(pattern.test('11222s')).toBeTruthy();
    expect(pattern.test('11222')).toBeFalsy();
    expect(pattern.test('1')).toBeFalsy();
    expect(pattern.test('ć')).toBeTruthy();
  });

});

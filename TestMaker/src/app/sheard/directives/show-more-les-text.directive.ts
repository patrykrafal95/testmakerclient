import { Directive, Input, ElementRef, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[shrShowMoreLesText]'
})
export class ShowMoreLesTextDirective implements OnChanges {

  // tslint:disable-next-line:no-input-rename
  @Input('shrShowMoreLesText') ShowMoreLesText: string;

  // tslint:disable-next-line:no-input-rename
  @Input('shrShowMoreLesTextLength') ShowMoreLesTextCount = 99;

  private _elemetRef;

  constructor(private el: ElementRef) {
    this._elemetRef = el.nativeElement;
  }


  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes['ShowMoreLesText'] !== 'undefined') {
      const change = changes['ShowMoreLesText'];
      if (!change.isFirstChange()) {
        const text = this.fullText(change.currentValue);
        if (text.length >= 100) {
          const firstPart = this.cutText(text, 0, this.ShowMoreLesTextCount);
          const seconPart = this.cutText(text, this.ShowMoreLesTextCount, text.length);
          $(this._elemetRef).
            html(
              `<span class='firstPart'>
              ${firstPart}
              <a href='#' class='readMore' title='Kliknij aby zobaczyć wszystko'>...Więcej </a>
            </span>
            <span class='secondPart'>
              ${seconPart}
              <a href='#' class='readLess' title='Kliknij aby zobaczyć mniej'>Mniej </a>
            </span>
            `);
        }
      }
    }
    this.showMoreLessAction();
  }

  showMoreLessAction() {
    $('a.readMore,a.readLess').off('click').on('click', function (e) {
      e.preventDefault();

      if ($(this).hasClass('readMore')) {
        $(this).toggle();
        $(this).parent().next().toggle();
      } else if ($(this).hasClass('readLess')) {
        $(this).parent().prev().children().toggle();
        $(this).parent().toggle();
      }
    });
  }

  private fullText(text: string): string {
    return text;
  }

  private cutText(text: string, start: number, stop: number): string {
    return text.substring(start, stop);
  }

}

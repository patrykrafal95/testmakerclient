import { Directive, Input, ElementRef, OnInit, OnChanges, SimpleChanges, DoCheck } from '@angular/core';
import * as $ from 'jquery';
import { element } from '@angular/core/src/render3/instructions';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[shrShowMoreLessElements]'
})
export class ShowMoreLessElementsDirective implements OnChanges {

  // tslint:disable-next-line:no-input-rename
  @Input('shrShowMoreLessElements') NumberOfElements: number;
  private _elementRef;

  constructor(private _eR: ElementRef) {
    this._elementRef = this._eR.nativeElement;
  }


  //#region show elements after changes in component
  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes['NumberOfElements'] !== 'undefined') {
      const change = changes['NumberOfElements'];
      if (change.currentValue >= 5) {
        this.clearContent();
        this.showHide();
      } else {
        this.clearContent();
        // tslint:disable-next-line:no-shadowed-variable
        const element = $(this._elementRef);
        element.hide().fadeIn(() => $(this).addClass('animation-show-tr-elements'));
        element.removeClass('show-more-background');
      }
    }
    this.onShowAllElements();
  }
  private showHide() {
    const elem = $(this._elementRef);
    if (!elem.hasClass('how-more-background')) {
      elem.addClass('show-more-background');
      elem.css({ 'overflow-y': 'hidden' })
        .after(`<footer class="show-all">
              <a href="#" class="show-all-elements"> ${this.resultText()}
              </a> </footer>`);
    }

  }
  //#endregion

  //#region  clear show all content
  private clearContent() {
    const elem = $(this._elementRef);
    elem.next('.show-all').html('');
  }
  //#endregion

  //#region on click show all content
  onShowAllElements(): void {
    $('a.show-all-elements').off('click').on('click', function (e) {
      e.preventDefault();

      const elemenet = $(this);
      elemenet.parent().prev().hide().fadeIn(() => $(this).addClass('animation-show-tr-elements'));
      elemenet.fadeOut(() => $(this).parent().remove());
      elemenet.parent().prev('.show-more-background').removeClass('show-more-background');
    });
  }
  //#endregion

  //#region result text
  resultText() {
    return `Pokaż całą zawartość`;
  }
  //#endregion

}


import { element } from 'protractor';
import { Directive, OnInit, ElementRef, OnDestroy, OnChanges, AfterViewInit, Input, SimpleChanges, DoCheck } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[shrDetailsInformationTable]'
})
export class DetailsInformationTableDirective implements AfterViewInit, OnChanges, DoCheck {

  // tslint:disable-next-line:no-input-rename
  @Input('shrTableElems') TableElems: number;
  private _tableElems;
  private _element: ElementRef;

  constructor(private er: ElementRef) {
    this._element = er.nativeElement;
  }

  ngDoCheck() {
    this.tableOnSmallScreen();
  }

  ngOnChanges(changes: SimpleChanges) {

    if (typeof changes['TableElems'] !== 'undefined') {
      const change = changes['TableElems'];
      if (change.currentValue > 0) {
        $(window).on('resize', () => {
          this.tableOnSmallScreen();
        });
      }
    }
  }

  ngAfterViewInit() {
    this.tableOnSmallScreen();
  }

  tableOnSmallScreen() {

    /*let thead = $('table.data-details>thead>tr>th');
    let tbody = $('table.data-details>tbody>tr');*/

    const thead = $(this._element).children('thead').children('tr').children('th');
    const tbody = $(this._element).children('tbody').children('tr');

    if (thead.length > 0 && tbody.length > 0) {

      if ($(window).outerWidth() <= 1200) {
        this.showHideDetailsTd(tbody, thead);
      } else {
        this.showHideDetailsTd(tbody, thead);
      }
    }

  }

  showHideDetailsTd(tbody, thead) {
    for (let i = 0; i < tbody.length; i++) {
      const td = tbody.eq(i).children('td');
      for (let j = 0; j < td.length; j++) {
        const pom = td.eq(j).find('span');
        if (!pom.hasClass('content') && $(window).outerWidth() <= 1200) {
          td.eq(j).prepend(`
                  <span class="content">
                    <b> ${thead.eq(j).text().length > 0 ? thead.eq(j).text() + ' :' : ' '} &nbsp; </b>
                  </span>`);
        } else {
          if (pom.hasClass('content') && $(window).outerWidth() > 1200) {
            pom.remove('span.content');
          }
        }
      }
    }
  }


}

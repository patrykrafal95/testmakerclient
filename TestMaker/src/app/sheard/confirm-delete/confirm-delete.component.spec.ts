import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ConfirmDeleteComponent } from './confirm-delete.component';
describe('ConfirmDeleteComponent', () => {
  let component: ConfirmDeleteComponent;
  let fixture: ComponentFixture<ConfirmDeleteComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ConfirmDeleteComponent]
    });
    fixture = TestBed.createComponent(ConfirmDeleteComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it('emitter test set true', () => {
    // TODO event emitter tests
      spyOn(component.emitConfirm, 'emit');
      component.onComfirm();
      expect(component.emitConfirm.emit).toHaveBeenCalled();
      expect(component.emitConfirm.emit).toHaveBeenCalledWith(true);
  });

});

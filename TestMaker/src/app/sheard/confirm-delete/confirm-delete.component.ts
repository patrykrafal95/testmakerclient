import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'shar-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.scss']
})
export class ConfirmDeleteComponent implements OnInit {

  @Output()
  emitConfirm = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  onComfirm() {
    this.emitConfirm.emit(true);
    console.log('emit');
  }

}

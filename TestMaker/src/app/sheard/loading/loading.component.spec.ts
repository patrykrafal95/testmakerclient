import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { LoadingComponent } from './loading.component';
import { LoaderService } from 'src/app/core/service/loader.service';

describe('LoadingComponent', () => {
  let component: LoadingComponent;
  let fixture: ComponentFixture<LoadingComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoadingComponent],
      providers: [LoaderService]
    });
    fixture = TestBed.createComponent(LoadingComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it('loader service can be inject ', inject([LoaderService], (service: LoaderService) => {
    expect(service).toBeTruthy();
  }));

  it('loader service test default value', inject([LoaderService], (service: LoaderService) => {
    expect(service.getStartStopValue()).toBe(false);
  }));

  it('loader serivce set value test', inject([LoaderService], (service: LoaderService) => {
    service.setStart(true);
    expect(service.getStartStopValue()).toBe(true);

    service.setStart(false);
    expect(service.getStartStopValue()).toBe(false);

  }));

});


import { Component, OnInit, Input } from '@angular/core';
import { LoaderService } from 'src/app/core/service/loader.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'share-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  ls: LoaderService;

  constructor(private _ls: LoaderService) {
    this.ls = _ls;
  }

  ngOnInit() {
  }

}

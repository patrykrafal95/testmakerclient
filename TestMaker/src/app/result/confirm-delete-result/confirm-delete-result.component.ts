import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'res-confirm-delete-result',
  templateUrl: './confirm-delete-result.component.html',
  styleUrls: ['./confirm-delete-result.component.scss']
})
export class ConfirmDeleteResultComponent implements OnInit {

  @Output()
  emitConfirmDeleteReult = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  onComfirm() {
    this.emitConfirmDeleteReult.emit(true);
  }

}

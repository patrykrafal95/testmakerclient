import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeleteResultComponent } from './confirm-delete-result.component';

describe('ConfirmDeleteResultComponent', () => {
  let component: ConfirmDeleteResultComponent;
  let fixture: ComponentFixture<ConfirmDeleteResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDeleteResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeleteResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should emit value true', () => {
    spyOn(component.emitConfirmDeleteReult, 'emit');
    component.onComfirm();
    expect(component.emitConfirmDeleteReult.emit).toHaveBeenCalled();
    expect(component.emitConfirmDeleteReult.emit).toHaveBeenCalledWith(true);
  });

});

import { MessageService } from 'src/app/message.service';
import { ResultService } from './../service/result.service';
import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { Result } from '../model/result';
import { Quiz } from 'src/app/quiz/model/quiz';
import { Router } from '@angular/router';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'res-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss']
})
export class ResultListComponent implements OnInit, OnChanges {

  //#region  prop
  result: Result;
  selectedResult: Result;
  // tslint:disable-next-line:no-inferrable-types
  @Input()
  quiz: Quiz;
  results: Result[];
  title: string;
  //#endregion

  constructor(private _rs: ResultService,
    private _route: Router,
    private _ms: MessageService) { }

  //#region  life cycle angular
  ngOnInit() {
    this.results = [];
    this.result = <Result>{};
  }

  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes['quiz'] !== 'undefined') {
      const change = changes['quiz'];
      if (!change.isFirstChange()) {
        this.loadData();
      }
    }
  }
  private loadData() {
    this._rs
      .getAllResults(this.quiz.id)
      .subscribe(_ => {
        this.results = _;
      });
  }

  //#endregion

  //#region  on action
  onCreate() {
    console.log(`id ${this.result}`);
    this._route.navigate(['result/create', this.quiz.id]);
  }

  onEdit(result: Result) {
    this._route.navigate(['result/edit', result.id]);
  }

  onConfirmDelete(confirm: boolean) {
    console.log('kasowanie');
    if (confirm) {
      this._rs
        .removeResults(this.result.id)
        .subscribe(() => {
          this._ms.success('Skasowano pomyślnie');
          this.loadData();
        });
    }
  }

  onDelete(result: Result) {
    this.result = result;
  }

  onSelect(selectResult: Result) {
    this.selectedResult = selectResult;
  }
  //#endregion

}

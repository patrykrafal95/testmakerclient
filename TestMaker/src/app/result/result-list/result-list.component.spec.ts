import { Config } from 'protractor';
import { ResultService } from './../service/result.service';
import { MessageService } from './../../message.service';
import { Result } from '../model/result';
import { Router, ActivatedRoute } from '@angular/router';
import { ResultListComponent } from './result-list.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CONFIG } from 'src/app/connection-string';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
describe('ResultListComponent', () => {
  let component: ResultListComponent;
  let fixture: ComponentFixture<ResultListComponent>;
  let resultService: ResultService;
  const config: Config = {
    apiUrl: 'http://localhost:11800/api/Result'
  };
  let httpTesing: HttpTestingController;
  beforeEach(() => {
    const messageServiceStub = { success: () => ({}) };
    const routerStub = { navigate: () => ({}) };
    const activatedRouteStub = {};
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        HttpClientTestingModule
      ],
      declarations: [ResultListComponent],
      providers: [
        { provide: ResultService, useClass: ResultService },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: CONFIG, useValue: config },
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    });
    fixture = TestBed.createComponent(ResultListComponent);
    component = fixture.componentInstance;
    resultService = TestBed.get(ResultService);
    httpTesing = TestBed.get(HttpTestingController);
  });

  it('can load instance ', () => {
    expect(component).toBeTruthy();
  });

  it('model test' , () => {

    const result: Result = {
      id: 1,
      quizId: 1,
      text: 'fddf'
    };


    const expectedResult: Result = {
      id: result.id,
      quizId: result.quizId,
      text: result.text
    };

    const resultNotUndefindedVlue: Result = {
      id: 1,
      quizId: 1,
      text: 'fddf',
      maxValue: 1,
      minValue: 2
    };

    expect(result.id).toBe(expectedResult.id);
    expect(result.quizId).toBe(expectedResult.quizId);
    expect(result.text).toBe(expectedResult.text);
    expect(result.maxValue).toBeUndefined();
    expect(result.minValue).toBeUndefined();
    expect(resultNotUndefindedVlue.maxValue).toBeDefined();
    expect(resultNotUndefindedVlue.minValue).toBeDefined();
  });

  it('htpp result service tests', () => {


    let r: Result = <Result>{};

    resultService.getResultById(1).subscribe(_ => {
      r = _;
    });


  });

});

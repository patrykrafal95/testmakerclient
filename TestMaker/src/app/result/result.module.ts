import { SharedModule } from './../sheard/sheard.module';
import { NgModule } from '@angular/core';
import { ResultListComponent } from './result-list/result-list.component';
import { ResultEditComponent } from './result-edit/result-edit.component';
import { ConfirmDeleteResultComponent } from './confirm-delete-result/confirm-delete-result.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ResultListComponent,
    ResultEditComponent,
    ConfirmDeleteResultComponent
  ],
  exports: [
    ResultListComponent,
    ResultEditComponent
  ]
})
export class ResultModule { }

import { Answer } from 'src/app/answer/model/answer';
import { HttpClient } from '@angular/common/http';
import { CONFIG } from 'src/app/connection-string';
import { Injectable, Inject } from '@angular/core';
import { Config } from 'protractor';
import { Result } from '../model/result';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(@Inject(CONFIG) private _conn: Config,
  private _ht: HttpClient) { }

  getAllResults(id: number) {
    return this._ht.get<Result[]>(`${this._conn.apiUrl}/Result/AllResult/${id}`);
  }
  removeResults(id: number) {
    return this._ht.get<Result>(`${this._conn.apiUrl}/Result/RemoveResult/${id}`);
  }

  addResult(result: Result) {
    return this._ht.post<Result>(`${this._conn.apiUrl}/Result/AddResult`, result);
  }

  updateResult(result: Result) {
    return this._ht.post<Result>(`${this._conn.apiUrl}/Result/UpdateResult`, result);
  }

  getResultById(id: number) {
    return this._ht.get<Result>(`${this._conn.apiUrl}/Result/GetResult/${id}`);
  }



}

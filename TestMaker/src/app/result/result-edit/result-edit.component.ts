import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { Component, OnInit } from '@angular/core';
import { Result } from '../model/result';
import { ActivatedRoute, Router } from '@angular/router';
import { ResultService } from '../service/result.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'res-result-edit',
  templateUrl: './result-edit.component.html',
  styleUrls: ['./result-edit.component.scss']
})
export class ResultEditComponent implements OnInit {

  //#region  prop
  title: string;
  result: Result;
  editMode: boolean;
  form: FormGroup;
  //#endregion

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _rs: ResultService,
    private _ms: MessageService,
    private _fb: FormBuilder) { }

  //#region on init
  ngOnInit() {
    this.result = <Result>{};
    this.createForm();
    const id = +this._activatedRoute.snapshot.params['id'];
    this.editMode = (this._activatedRoute.snapshot.url[1].path === 'edit');

    if (this.editMode) {
      this._rs.getResultById(id).subscribe(_ => {
        this.result = _;
        this.title = `Edycja -  ${this.result.text}`;
        this.updateForm();
      });
    } else {
      this.result.quizId = id;
      this.title = 'Utwórz nowy wynik';
    }

  }
  //#endregion

  //#region  on submit form
  onSubmit() {

    const resultBuf = <Result>{};
    resultBuf.text = this.form.value.text;
    resultBuf.maxValue = this.form.value.maxValue;
    resultBuf.minValue = this.form.value.minValue;
    resultBuf.quizId = this.result.quizId;

    if (this.editMode) {
      resultBuf.id = this.result.id;
      this._rs.updateResult(resultBuf).subscribe(_ => {
        const q = _;
        this._ms.success('Zaktualizowano pomyślnie');
        this._router.navigate(['quiz/edit', q.quizId]);
      });
    } else {
      this._rs.addResult(resultBuf).subscribe(_ => {
        this._ms.success('Dodano pomyślnie');
        const q = _;
        this._router.navigate(['quiz/edit', q.quizId]);
      });
    }
  }
  //#endregion

  //#region on back
  onBack() {
    this._router.navigate(['quiz/edit', this.result.quizId]);
  }
  //#endregion

  //#region  form create update
  createForm() {
    this.form = this._fb.group({
      text: ['', Validators.required],
      minValue: ['', Validators.pattern(/^\d*$/)],
      maxValue: ['', Validators.pattern(/^\d*$/)]
    });
  }

  updateForm() {
    this.form.setValue({
      text: this.result.text,
      minValue: typeof this.result.minValue === 'undefined' ? '' : this.result.minValue,
      maxValue: typeof this.result.maxValue === 'undefined' ? '' : this.result.maxValue,
    });
  }
  //#region

  //#region  form validate state
  getFormControlName(name: string) {
    return this.form.get(name);
  }

  isValid(name: string) {
    const e = this.getFormControlName(name);
    return e && (e.touched || e.dirty) && e.valid;
  }

  hasError(name: string) {
    const e = this.getFormControlName(name);
    return e && (e.touched || e.dirty) && !e.valid;
  }
  //#endregion


}

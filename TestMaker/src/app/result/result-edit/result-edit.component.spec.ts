import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ResultService } from '../service/result.service';
import { ResultEditComponent } from './result-edit.component';
import { LoaderService } from 'src/app/core/service/loader.service';

describe('ResultEditComponent', () => {
  let component: ResultEditComponent;
  let fixture: ComponentFixture<ResultEditComponent>;
  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ResultEditComponent],
      providers: [
        { provide: FormBuilder },
        { provide: LoaderService },
        { provide: MessageService },
        { provide: ActivatedRoute },
        { provide: Router },
        { provide: ResultService }
      ]
    });
    fixture = TestBed.createComponent(ResultEditComponent);
    component = fixture.componentInstance;

    component.form = new FormGroup({
      text: new FormControl('', [Validators.required]),
      minValue: new FormControl('', [Validators.pattern(/^\d*$/)]),
      maxValue: new FormControl( '', [Validators.pattern(/^\d*$/)])
    });

  });

  afterEach(() => {
    fixture = null;
    component = null;
  });

  it('can load instane ' , () => {
    expect(component).toBeTruthy();
  });

  it('check component form is not valid default' , () => {

    expect(component.form.valid).toBeFalsy();
  });

  it('check component form is not valid' , () => {
    // TODO form validator
    const text = component.form.controls['text'];
    const minValue = component.form.controls['minValue'];
    const maxValue = component.form.controls['maxValue'];

    text.setValue('');
    minValue.setValue('4h');
    maxValue.setValue('4h');

    expect(text.valid).toBeFalsy();
    expect(minValue.valid).toBeFalsy();
    expect(maxValue.valid).toBeFalsy();

  });

  it('check component form is valid' , () => {

    const text = component.form.controls['text'];
    const minValue = component.form.controls['minValue'];
    const maxValue = component.form.controls['maxValue'];

    text.setValue('e');
    minValue.setValue('4');
    maxValue.setValue('4');

    const status = component.getFormControlName('text');

    expect(status.valid).toBe(true);
    expect(text.valid).toBeTruthy();
    expect(minValue.valid).toBeTruthy();
    expect(maxValue.valid).toBeTruthy();

  });



});

import { QuestionEditComponent } from './question/question-edit/question-edit.component';
import { AnswerEditComponent } from './answer/answer-edit/answer-edit.component';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { NgProgressRouterModule } from '@ngx-progressbar/router';

import { QuizComponent } from './quiz/quiz/quiz.component';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './core/home/home.component';
import { LoginComponent } from './login/login/login.component';
import { QuizEditComponent } from './quiz/quiz-edit/quiz-edit.component';
import { ResultEditComponent } from './result/result-edit/result-edit.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'quiz/create', component: QuizEditComponent },
  { path: 'quiz/:id', component: QuizComponent},
  { path: 'question/create/:id', component: QuestionEditComponent},
  { path: 'question/edit/:id', component: QuestionEditComponent},
  { path: 'quiz/edit/:id', component: QuizEditComponent},
  { path: 'answer/create/:id', component: AnswerEditComponent},
  { path: 'answer/edit/:id', component: AnswerEditComponent},
  { path: 'result/create/:id', component: ResultEditComponent},
  { path: 'result/edit/:id', component: ResultEditComponent},
  { path: 'login', component: LoginComponent },
  { path: '**', component: HomeComponent}
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      progressAnimation: 'decreasing',
      positionClass: 'toast-center-center'
    }),
    [RouterModule.forRoot(routes)],
    NgProgressModule,
    NgProgressHttpModule,
    NgProgressRouterModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, AfterViewInit, OnInit } from '@angular/core';
import { Inject } from '@angular/core';
import * as $ from 'jquery';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  date: Date = new Date();

  constructor() { }

  ngAfterViewInit() {
    this.checkConnectionStatus();
  }

  //#region connection status
  checkConnectionStatus() {
    const status = $('div#status');
    const checkStatus = () => {
      const con = ((navigator.onLine) ?
        'online' : 'offline');
      if (con === 'online') {
        status.fadeOut('slow');
      } else {
        if (con === 'offline') {
          status.fadeIn('slow');
        }
      }
    };
    checkStatus();
    $(window).off('online').on('online', () => {
      checkStatus();
    });
    console.log(window);
    $(window).off('offline').on('offline', () => {
      checkStatus();
    });
  }
  //#endregion


}


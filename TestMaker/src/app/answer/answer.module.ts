import { SharedModule } from './../sheard/sheard.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnswerListComponent } from './answer-list/answer-list.component';
import { AnswerEditComponent } from './answer-edit/answer-edit.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [AnswerListComponent, AnswerEditComponent],
  exports: [
    AnswerListComponent,
    AnswerEditComponent
  ]
})
export class AnswerModule { }

import { MessageService } from './../../message.service';
import { Router } from '@angular/router';
import { AnswerService } from './../service/answer.service';
import { Question } from './../../question/model/question';
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Answer } from '../model/answer';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ans-answer-list',
  templateUrl: './answer-list.component.html',
  styleUrls: ['./answer-list.component.scss']
})
export class AnswerListComponent implements OnInit, OnChanges {

  answerSelected: Answer;
  confirmAnswerDelete: Answer;
  @Input() question: Question;
  answers: Answer[];
  title: string;

  constructor(private _as: AnswerService,
    private _router: Router,
    private _ms: MessageService) {}

  ngOnInit() {
    this.answers = [];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes['question'] !== 'undefined') {
      const change = changes['question'];
      if (!change.isFirstChange()) {
        this.loadData();
      }
    }
  }

  private loadData() {
    this._as.getAllAnswerById(this.question.id).subscribe(_ => {
      this.answers = _;
    });
  }

  onCreate() {
    this._router.navigate(['answer/create', this.question.id]);
  }

  onEdit(answer: Answer) {
    this._router.navigate(['answer/edit', answer.id]);
  }

  onDelete(answer: Answer) {
    this.confirmAnswerDelete = answer;
  }

  onConfirmDelete(confirm: boolean) {
    if (confirm) {
      this._as.removeAnswer(this.confirmAnswerDelete).subscribe(_ => {
        this.loadData();
        this._ms.success('Skasowano pomyślnie');
      });
    }
  }

  onSelect(selected: Answer) {
    this.answerSelected = selected;
  }


}

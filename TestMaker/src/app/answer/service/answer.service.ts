import { Config } from './../../connection-string';
import { Injectable, Inject } from '@angular/core';
import { CONFIG } from 'src/app/connection-string';
import { Answer } from '../model/answer';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private _hs: HttpClient,
    @Inject(CONFIG) private _conn: Config) { }

  getAllAnswerById(id: number) {
    return this._hs.get<Answer[]>(`${this._conn.apiUrl}/Answer/AllAnswers/${id}`);
  }

  getAnswerById(id: number) {
    return this._hs.get<Answer>(`${this._conn.apiUrl}/Answer/GetAnswer/${id}`);
  }
  addAnswer(answer: Answer) {
    return this._hs.post<Answer>(`${this._conn.apiUrl}/Answer/AddAnswer`, answer);
  }
  updateAnswer(answer: Answer) {
    return this._hs.post<Answer>(`${this._conn.apiUrl}/Answer/UpdateAnswer` , answer);
  }

  removeAnswer(answer: Answer) {
    return this._hs.get<Answer>(`${this._conn.apiUrl}/Answer/RemoveAnswer/${answer.id}`);
  }

}

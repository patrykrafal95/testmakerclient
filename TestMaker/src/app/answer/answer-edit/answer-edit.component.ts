import { AnswerService } from './../service/answer.service';
import { Answer } from './../model/answer';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'src/app/message.service';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'ans-answer-edit',
  templateUrl: './answer-edit.component.html',
  styleUrls: ['./answer-edit.component.scss']
})
export class AnswerEditComponent implements OnInit {
  //#region  prop
  title: string;
  answer: Answer;
  editMode: boolean;
  form: FormGroup;
  //#endregion

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _as: AnswerService,
    private _ms: MessageService,
    private _location: Location,
    private _fb: FormBuilder,
  ) { }

  //#region  onInit
  ngOnInit() {
    this.answer = <Answer>{};
    this.createForm();
    const id = +this._activatedRoute.snapshot.params['id'];
    this.editMode = this._activatedRoute.snapshot.url[1].path === 'edit';
    this.detectedEditMode(id);
  }

  private detectedEditMode(id: number) {
    if (this.editMode) {
      this._as.getAnswerById(id).subscribe(_ => {
        this.answer = _;
        this.title = `Edycja ${this.answer.text}`;
        this.updateForm();
      });
    } else {
      this.answer.questionId = id;
      this.title = 'Utwórz nową odpowiedź';
    }
  }
  //#endregion

  //#region  onSubmit
  onSubmit() {
    const bufAnswer = <Answer>{};
    bufAnswer.id = this.answer.id;
    bufAnswer.questionId = this.answer.questionId;
    bufAnswer.text = this.form.value.text;
    bufAnswer.value = this.form.value.value;


    if (this.editMode) {
      this._as.updateAnswer(bufAnswer).subscribe(_ => {
        const a = _;
        this._ms.success('Zaktualizowano pomyślnie');
        this._router.navigate(['question/edit', a.questionId]);
      });
    } else {
      this._as.addAnswer(bufAnswer).subscribe(_ => {
        this._ms.success('Dodano pomyślnie');
        const a = _;
        this._router.navigate(['question/edit', a.questionId]);
      });
    }
  }
  //#endregion

  //#region  onBack
  onBack() {
    this._location.back();
  }
  //#endregion

  //#region  formVlidation
  createForm() {
    this.form = this._fb.group({
      text: ['', Validators.required],
      value: ['',
        [
          Validators.required,
          Validators.min(-5),
          Validators.max(5)
        ]
      ]
    });
  }

  updateForm() {
    this.form.setValue({
      text: this.answer.text,
      value: this.answer.value
    });
  }
  //#endregion

  //#region form error
  getFormControl(name: string) {
    return this.form.get(name);
  }

  isValid(name: string) {
    const e = this.getFormControl(name);
    return e && e.valid;
  }

  hasError(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched) && !e.valid;
  }
  //#endregion
}

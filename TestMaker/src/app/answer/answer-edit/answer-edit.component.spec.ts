import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';
import { AnswerEditComponent } from './answer-edit.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AnswerService } from '../service/answer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'src/app/message.service';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { LoaderService } from 'src/app/core/service/loader.service';

describe('AnswerEditComponent', () => {
  let component: AnswerEditComponent;
  let fixture: ComponentFixture<AnswerEditComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [AnswerEditComponent],
      providers: [
        { provide: LoaderService },
        { provide: AnswerService },
        { provide: ActivatedRoute },
        { provide: Router },
        { provide: MessageService },
        { provide: Location },
        { provide: FormBuilder }
      ]
    });
    fixture = TestBed.createComponent(AnswerEditComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      text: new FormControl('', Validators.required),
      value: new FormControl(
        '', [Validators.required, Validators.min(-5), Validators.max(5)]
      )
    });
  });

  afterEach(() => {
    fixture = null;
    component = null;
  });

  it('should be create', () => {
    expect(component).toBeTruthy();
  });

  it('form is invalid by default', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('form valid data min', () => {
    const text = component.form.controls['text'];
    const value = component.form.controls['value'];

    text.setValue('1');
    value.setValue(-5);

    expect(component.form.valid).toBeTruthy();
  });

  it('form valid data max', () => {
    const text = component.form.controls['text'];
    const value = component.form.controls['value'];

    text.setValue('1');
    value.setValue(5);

    expect(component.form.valid).toBeTruthy();
  });

  it('form invalid data min', () => {
    const text = component.form.controls['text'];
    const value = component.form.controls['value'];

    text.setValue('');
    value.setValue(-6);

    expect(component.form.valid).toBeFalsy();
  });

  it('form invalid data max', () => {
    const text = component.form.controls['text'];
    const value = component.form.controls['value'];

    text.setValue('');
    value.setValue(6);

    expect(component.form.valid).toBeFalsy();
  });

});

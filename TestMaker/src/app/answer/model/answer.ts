export interface Answer {
  id: number;
  questionId: number;
  text: string;
  value?: number;
}

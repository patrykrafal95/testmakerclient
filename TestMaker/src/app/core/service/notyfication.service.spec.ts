import { TestBed, inject } from '@angular/core/testing';

import { NotyficationService } from './notyfication.service';

describe('NotyficationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotyficationService]
    });
  });

  it('should be created', inject([NotyficationService], (service: NotyficationService) => {
    expect(service).toBeTruthy();
  }));
});

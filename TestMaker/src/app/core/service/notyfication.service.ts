import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotyficationService {

  private _msg: string;

  constructor() { }

  setMsg(msg: string = '') {
    this._msg = msg;
  }

  getMsg() {
    return this._msg;
  }

}

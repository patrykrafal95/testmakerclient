import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { error } from '@angular/compiler/src/util';
import { map, filter, tap } from 'rxjs/operators';
import { MessageService } from 'src/app/message.service';
import { Router } from '@angular/router';
import { LoaderService } from './loader.service';

@Injectable()
export class ErrorHandlingInterceptor implements HttpInterceptor {


  constructor(private messageService: MessageService,
    private _route: Router,
    private _ls: LoaderService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // console.log(req);
    return next.handle(req).pipe(
      tap(event => {

        if (req) {
          this._ls.setStart(true);
        } else if (event instanceof HttpErrorResponse) {
          this.errorConection('Ups... wystąpił błąd');
        }
        // tslint:disable-next-line:no-shadowed-variable
      }, error => {
        console.log(error);
        const status = error['status'];

        if (status === 400) {
          this.errorConection('Sprawdź czy wszystkie dane zostały wprowadzone poprawnie');
        } else if (status === 404) {
          this.errorConection('Nie odnaleziono zasobu');
        } else {
          this.errorConection('Ups... wystąpił błąd');
        }
        this._ls.setStart(false);
      }, () => {
        this._ls.setStart(false);
      }));
  }

  private errorConection(text: string) {
    // this._route.navigate(['home']);
    this.messageService.error(text);
  }
}

import { TestBed, inject } from '@angular/core/testing';

import { LoaderService } from './loader.service';

describe('LoaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoaderService]
    });
  });
  it('should be created', inject([LoaderService], (service: LoaderService) => {
    expect(service).toBeTruthy();
  }));

  it('loader service Test', inject([LoaderService], (service: LoaderService) => {

     expect(service.getStartStopValue()).toBe(false);

      service.setStart(true);
      expect(service.getStartStopValue()).toBe(true);

      service.setStart(false);
      expect(service.getStartStopValue()).toBe(false);

  }));

});

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private isStarted = false;

  constructor() { }

  setStart(start: boolean = false): void {
    this.isStarted = start;
  }

  getStartStopValue(): boolean {
    return this.isStarted;
  }


}

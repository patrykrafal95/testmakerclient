import { NotyficationService } from './../service/notyfication.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotyficationComponent } from './notyfication.component';

describe('NotyficationComponent', () => {
  let component: NotyficationComponent;
  let fixture: ComponentFixture<NotyficationComponent>;
  let notiService: NotyficationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotyficationComponent ],
      providers: [NotyficationService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotyficationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    notiService = TestBed.get(NotyficationService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check noti service is value', () => {
    notiService.setMsg('test');
    expect(notiService.getMsg()).toBe('test');
  });

  it('check noti service is no value', () => {
    notiService.setMsg();
    expect(notiService.getMsg()).toBe('');
  });

  it('check datetimme format with 0 ', () => {
   const date = new Date();
   date.setUTCFullYear(2019, 6, 31);
   date.setHours(10);
   date.setMinutes(1);

   expect(component.dateTimeFormat(date)).toBe('31.07.2019 10:01');

  });

  it('check datetimme format with out 0 ', () => {
    const date = new Date();
    date.setUTCFullYear(2019, 6, 31);
    date.setHours(10);
    date.setMinutes(12);

    expect(component.dateTimeFormat(date)).toBe('31.07.2019 10:12');

   });

});

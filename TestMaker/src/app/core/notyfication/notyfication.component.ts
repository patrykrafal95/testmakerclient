import { NotyficationService } from './../service/notyfication.service';
import { Component, OnInit, DoCheck } from '@angular/core';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'shr-notyfication',
  templateUrl: './notyfication.component.html',
  styleUrls: ['./notyfication.component.scss']
})
export class NotyficationComponent implements OnInit, DoCheck {

  constructor(private _ns: NotyficationService) { }

  ngDoCheck() {
    this.notyficationContextText();
    this.checkNotiDataIsEmptyAndVisible();

  }



  ngOnInit() {
    this.notyficationOnClick();
    this.removeNotiIteam();

  }

  //#region check data is empty and visible
  checkNotiDataIsEmptyAndVisible() {
    const notiContent = $('.noti-container .noti-content');
    if ($('.noti-container .noti-content .noti-data').children().length <= 0
      && notiContent.is(':visible')) {
      notiContent.fadeOut();
      notiCounterSet();
    }
  }
  //#endregion


  //#region remove iteam noti
  removeNotiIteam() {
    $('body').on('click', '.noti-content .card .card-noti-header-container .remove-iteam-noti', function (e) {

      const elem = $(this);
      elem.closest('.card').fadeOut(function () {
        $(this).remove();
      });
    });
  }
  //#endregion

  //#region on click  notyfication
  notyficationOnClick() {
    $('.noti-container .noti').off('click').on('click', function (e) {
      e.stopPropagation();

      const notiContent = $('.noti-container .noti-content');
      if ($('.noti-content .noti-data').children().length <= 0
        && !notiContent.is(':visible')) {
        return false;
      } else if (!notiContent.is(':visible')) {
        notiContent.css({ 'display': 'flex' }).fadeIn();
      } else {
        notiContent.fadeOut();
      }
      notiCounterSet();
    });
  }
  //#endregion

  //#region  notyfiction content text
  notyficationContextText() {
    if (this._ns.getMsg() && this._ns.getMsg().length > 0) {
      this.notyficationText(this._ns.getMsg());
      this._ns.setMsg();
    }
  }
  //#endregion

  //#region notyfication text
  notyficationText(change: string) {
    const data = new Date();

    $(`
        <div class="card">
           <div class="card-block">
              <div class="card-text">
                  <div class='card-noti-header-container'>
                    <div class='noti-info'>
                      ${change}
                    </div>
                    <div class='remove-iteam-noti'>
                      <i class="fa fa-trash" aria-hidden="true" title='Kasuj'></i>
                    </div>
                  </div>
                </div>
                <div class="card-text">
                    ${this.dateTimeFormat(data)}
                </div>
              </div>
          </div>
    `).prependTo('.noti-data');
  }
  //#endregion


  //#region date format
  dateTimeFormat(data: Date) {
    return `${data.toLocaleDateString()} ${data.getHours()}:${data.getMinutes() < 10 ? '0' + data.getMinutes() : data.getMinutes()}`;
  }
  //#endregion

}

//#region noticontent set count 0
function notiCounterSet() {
  const noti = $('.noti-container .noti .noti-count');
  if (noti.text() !== '0' &&
    $('.noti-container .noti-content').not(':visible')) {
    noti.fadeOut(function () {
      $(this).text(0).fadeIn();
    });
  }
}
//#endregion


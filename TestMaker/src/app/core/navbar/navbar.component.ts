import { NotyficationService } from './../service/notyfication.service';
import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'core-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


  private _hubConn: HubConnection;

  constructor(private _ns: NotyficationService) { }

  ngOnInit() {

    this.notyficationConnetion();

  }

  //#region  notyfication connection
  notyficationConnetion() {
    this._hubConn = new HubConnectionBuilder().withUrl('http://localhost:11800/notify').build();
    this._hubConn.start()
      .then(() => console.log('Połączenie urucchomione'))
      .catch(() => console.log('Wystąpił błąd'));
    this._hubConn.on('SendNotifyToAll', (msg: string) => {
      this.updateCounter('.noti-container .noti-count');
      this._ns.setMsg(msg);
    });
  }
  //#endregion

  //#region  update counter
  updateCounter(elem: string) {
    // tslint:disable-next-line:radix
    const count = this.counterAdd($(elem).text());
    $(elem).fadeOut().fadeOut(function () {
      $(this).text(count).fadeIn();
    });
  }

  counterAdd(elem: string) {
    // tslint:disable-next-line:radix
    return parseInt(elem) + 1 || 0;
  }
  //#endregion
}

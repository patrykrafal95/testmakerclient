import { NgModule } from '@angular/core';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { QuizModule } from '../quiz/quiz.module';
import { Config, CONFIG } from '../connection-string';
import { LoginModule } from '../login/login.module';
import { NotyficationComponent } from './notyfication/notyfication.component';
import { NotyficationService } from './service/notyfication.service';
import { MessageService } from '../message.service';
import { ErrorHandlingInterceptor } from './service/ErrorHandlingInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ResultService } from '../result/service/result.service';
import { QuizService } from '../quiz/service/quiz.service';
import { QuestionService } from '../question/service/question.service';
import { AnswerService } from '../answer/service/answer.service';
import { LoaderService } from './service/loader.service';


const config: Config = {
  apiUrl: 'http://localhost:11800/api'
};

@NgModule({
  imports: [
    RouterModule,
    QuizModule,
    LoginModule
  ],
  declarations: [NavbarComponent, HomeComponent, NotyficationComponent],
  exports: [
    NavbarComponent,
    HomeComponent
  ],
  providers: [
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlingInterceptor,
      multi: true
    },
    { provide: CONFIG, useValue: config },
    NotyficationService,
    ResultService,
    QuizService,
    QuestionService,
    AnswerService,
    LoaderService
  ]
})
export class CoreModule { }

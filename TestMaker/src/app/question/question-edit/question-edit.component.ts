import { QuestionService } from './../service/question.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Question } from './../model/question';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/message.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'que-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.scss']
})
export class QuestionEditComponent implements OnInit {

  //#region  prop
  title: string;
  question: Question;
  editMode: boolean;
  form: FormGroup;
  //#endregion

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _qs: QuestionService,
    private _ms: MessageService,
    private _fb: FormBuilder) { }

  //#region  on init
  ngOnInit() {
    this.question = <Question>{};
    this.createForm();
    const id = +this._activatedRoute.snapshot.params['id'];
    this.editMode = (this._activatedRoute.snapshot.url[1].path === 'edit');
    this.editCreateForm(id);

  }
  private editCreateForm(id: number) {
    if (this.editMode) {
      this._qs.getQuestionById(id).subscribe(_ => {
        this.question = _;
        this.title = `Edycja -  ${this.question.text}`;
        this.updateForm();
      });
    } else {
      this.question.quizId = id;
      this.title = 'Utwórz nowe pytanie';
    }
  }

  //#endregion

  //#region click action
  onSubmit() {

    const bufAnswer = <Question>{};

    bufAnswer.text = this.form.value.text;
    bufAnswer.quizId = this.question.quizId;

    if (this.editMode) {
      bufAnswer.id = this.form.value.id;

      this._qs.updateQuestion(bufAnswer).subscribe(_ => {
        const q = _;
        this._ms.success('Zaktualizowano pomyślnie');
        this._router.navigate(['quiz/edit', q.quizId]);
      });
    } else {
      this._qs.addQuestion(bufAnswer).subscribe(_ => {
        this._ms.success('Dodano pomyślnie pomyślnie');
        const q = _;
        this._router.navigate(['quiz/edit', q.quizId]);
      });
    }
  }

  onBack() {
    this._router.navigate(['quiz/edit', this.question.quizId]);
  }
  //#endregion

  //#region  form validate
  createForm() {
    this.form = this._fb.group({
      text: ['', Validators.required],
      id: this.question.id,
      quizId: this.question.quizId
    });
  }

  updateForm() {
    this.form.setValue({
      text: this.question.text,
      id: this.question.id,
      quizId: this.question.quizId
    });
  }
  //#endregion

  //#region  form error
  getFormControl(name: string) {
    return this.form.get(name);
  }

  isValid(name: string) {
    const e = this.getFormControl(name);
    return e && e.valid;
  }

  hasError(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched) && !e.valid;
  }
  //#endregion



}

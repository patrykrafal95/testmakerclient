import { SharedModule } from './../../sheard/sheard.module';
import { ConfirmDeleteComponent } from './../../sheard/confirm-delete/confirm-delete.component';
import { AnswerListComponent } from './../../answer/answer-list/answer-list.component';
import { LoadingComponent } from './../../sheard/loading/loading.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionEditComponent } from './question-edit.component';
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from 'selenium-webdriver/http';
import { QuizService } from 'src/app/quiz/service/quiz.service';
import { MessageService } from 'src/app/message.service';
import { QuestionService } from '../service/question.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { LoaderService } from 'src/app/core/service/loader.service';

describe('question edit tests ', () => {
  let component: QuestionEditComponent;
  let fixture: ComponentFixture<QuestionEditComponent>;
  let erros = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [QuestionEditComponent],
      providers: [
        { provide: QuestionService},
        { provide: Router },
        { provide: ActivatedRoute},
        { provide: MessageService},
        { provide: LoaderService },
        { provide: FormBuilder }
      ]
    });

    fixture = TestBed.createComponent(QuestionEditComponent);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      text: new FormControl('', [Validators.required]),
      id: new FormControl(),
      quizId: new FormControl()
    });
  });

  afterEach(() => {
    component = null;
    fixture = null;
  });

  it('should load instance', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty ', () => {
    const text = component.form.controls['text'];
    text.setValue('');

    erros = text.errors || {};

    expect(erros['requried']).toBeFalsy();
    expect(component.form.invalid).toBeTruthy();
  });

  it('form valid when text is not empty', () => {
    const text = component.form.controls['text'];
    const id = component.form.controls['id'];
    const quizId = component.form.controls['quizId'];

    erros = text.errors || {};
    text.setValue('s');
    id.setValue(1);
    quizId.setValue(2);

    expect(component.hasError('text')).toBeFalsy();
    expect(component.isValid('text')).toBeTruthy();
    expect(component.form.value.text).toBe('s');
    expect(component.form.value.id).toBe(1);
    expect(component.form.value.quizId).toBe(2);
    expect(erros['required']).toBeTruthy();
    expect(component.form.invalid).toBeFalsy();
  });

});

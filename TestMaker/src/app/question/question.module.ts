import { NgModule } from '@angular/core';
import { QuestionListComponent } from './question-list/question-list.component';
import { SharedModule } from '../sheard/sheard.module';
import { QuestionEditComponent } from './question-edit/question-edit.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [QuestionListComponent, QuestionEditComponent],
  exports: [
    QuestionListComponent,
    QuestionEditComponent
  ]
})
export class QuestionModule { }

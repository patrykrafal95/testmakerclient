import { Router } from '@angular/router';
import { QuestionService } from './../service/question.service';
import { Question } from './../model/question';
import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Quiz } from 'src/app/quiz/model/quiz';
import { MessageService } from 'src/app/message.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'que-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss']
})
export class QuestionListComponent implements OnInit, OnChanges {
  //#region  prop
  question: Question;
  selectedQuestion: Question;
  // tslint:disable-next-line:no-inferrable-types
  @Input()
  quiz: Quiz;
  questions: Question[];
  title: string;
  //#endregion

  constructor(private _qs: QuestionService,
    private _route: Router,
    private _ms: MessageService) {}

  //#region  life cycle angular
  ngOnInit() {
    this.questions = [];
    this.question = <Question>{};
  }

  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes['quiz'] !== 'undefined') {
      const change = changes['quiz'];
      if (!change.isFirstChange()) {
        this.loadData();
      }
    }
  }
  private loadData() {
    this._qs
      .getAllQuestions(this.quiz.id)
      .subscribe(_ => {
        this.questions = _;
      });
  }

  //#endregion

  //#region  on action
  onCreate() {
    this._route.navigate(['question/create', this.quiz.id]);
  }

  onEdit(question: Question) {
    this._route.navigate(['question/edit', question.id]);
  }

  onConfirmDelete(confirm: boolean) {
    if (confirm) {
      this._qs
        .removeQuestion(this.question.id)
        .subscribe(() => {
          this._ms.success('Skasowano pomyślnie');
          this.loadData();
        });
    }
  }

  onDelete(question: Question) {
    this.question = question;
  }

  onSelect(selected: Question) {
    this.selectedQuestion = selected;
  }

  //#endregion
}

import { CONFIG } from 'src/app/connection-string';
import { Config } from './../../connection-string';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Question } from '../model/question';


@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private _hs: HttpClient,
    @Inject(CONFIG) private _conn: Config) { }


    getAllQuestions(id: number) {
      return this._hs.get<Question[]>(`${this._conn.apiUrl}/question/AllQuiz/${id}`);
    }

    removeQuestion(id: number) {
      return this._hs.get<Question>(`${this._conn.apiUrl}/question/RemoveQuestion/${id}`);
    }

    getQuestionById(id: number) {
      return this._hs.get<Question>(`${this._conn.apiUrl}/question/GetQuestion/${id}`);
    }

    addQuestion(question: Question) {
      return this._hs.post<Question>(`${this._conn.apiUrl}/question/AddQuestion`, question);
    }

    updateQuestion(question: Question) {
      return this._hs.post<Question>(`${this._conn.apiUrl}/question/UpdateQuestion`, question);
    }

}

import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private _ts: ToastrService) { }

  success(msg: string) {
    this._ts.success(msg);
  }

  error(msg: string) {
    this._ts.error(msg);
  }

  warrning(msg: string) {
    this._ts.warning(msg);
  }

}

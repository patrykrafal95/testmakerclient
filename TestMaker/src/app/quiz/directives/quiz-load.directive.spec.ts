import { CONFIG, Config } from 'src/app/connection-string';
import { MessageService } from 'src/app/message.service';
import { SharedModule } from './../../sheard/sheard.module';
import { QuizListComponent } from './../quiz-list/quiz-list.component';
import { TestBed, ComponentFixture, fakeAsync } from '@angular/core/testing';
import { Component, DebugElement, ElementRef } from '@angular/core';
import { QuizLoadDirective } from './quiz-load.directive';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { QuizService } from '../service/quiz.service';
import { Router, ActivatedRoute } from '@angular/router';
import { By } from '@angular/platform-browser';
import * as $ from 'jquery';

describe('QuizLoadDirective', () => {
  let component: QuizListComponent;
  let fixture: ComponentFixture<QuizListComponent>;
  const config: Config = {
    apiUrl: 'http://localhost:11800/api/quiz'
  };
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, HttpClientTestingModule],
      providers: [
        { provide: QuizService, useClass: QuizService },
        { provide: MessageService },
        { provide: CONFIG, useValue: config },
        { provide: Router },
        { provide: ActivatedRoute }
      ],
      declarations: [QuizListComponent, QuizLoadDirective]
    });
    fixture = TestBed.createComponent(QuizListComponent);
    const bodyElem = $('body');
    bodyElem.append(`
    <ul class='quiz'>
      <li>fdfds</li>
      <li>ffdsfdsfdsf</li>
      <li>fds</li>
    </ul>`);
    component = fixture.componentInstance;
  });

  it('test directive', () => {

    const compiled = $('ul.quiz');
    expect(compiled).toBeTruthy();
    const el = $(compiled);
    const li = el.children('li');
    console.log(li.length);
    li.each(function(i) {
      const buf = $(this);
      buf.hide();
      buf.delay(i * 100).slideDown();
    });

    expect(component).toBeTruthy();
    expect(li.length).toBe(3);
  });
});

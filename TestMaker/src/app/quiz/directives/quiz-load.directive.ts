import { Directive, ElementRef, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import * as $ from 'jquery';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[quiQuizLoad]'
})
export class QuizLoadDirective implements AfterContentInit {


  constructor(private _er: ElementRef) { }


  ngAfterContentInit() {
    this.loadQuizAnimation();
  }

  loadQuizAnimation() {
    const li = this.ListElement();
    console.log(li.length);
    li.each(function(i) {
      const buf = $(this);
      buf.hide();
      buf.delay(i * 100).slideDown('fast');
    });
  }


  ListElement() {
    const el = $(this._er.nativeElement);
    const li = el.children('li');
    return li;
  }
}

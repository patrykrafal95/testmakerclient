import { Component, OnInit, Input, Inject } from '@angular/core';
import { QuizService } from '../service/quiz.service';
import { MessageService } from '../../message.service';
import { CONFIG, Config } from '../../connection-string';
import { Router, ActivatedRoute } from '@angular/router';
import { Quiz } from '../model/quiz';
import { LoaderService } from 'src/app/core/service/loader.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'qui-quiz-list',
  templateUrl: './quiz-list.component.html',
  styleUrls: ['./quiz-list.component.scss']
})

export class QuizListComponent implements OnInit {

  //#region  prop
  title: string;
  selectedQuiz: Quiz;
  quizzes: Quiz[] = [];
  @Input() class: string;
  ls: LoaderService;
  //#endregion

  //#region
  constructor(private _qs: QuizService,
    private _ms: MessageService,
    @Inject(CONFIG) private _config: Config,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _ls: LoaderService) {
      this.ls = _ls;
  }
  //#endregion

  //#region  onInit
  ngOnInit() {
    this.title = 'Lista quizów';
    // this.refresQuizList();
    this.quizAfterClass();
  }
  //#endregion

  //#region  getQuiz from service
  refresQuizList() {
    this._qs.getQuiz().subscribe(_ => {
      this.quizzes = _;
    });
  }
  //#endregion

  //#region  onSelect
  onSelect(quiz: Quiz) {
    this.selectedQuiz = quiz;
    console.log(`Wybrano quiz ${this.selectedQuiz.id}`);
    this._router.navigate(['/quiz', this.selectedQuiz.id]);
  }
  //#endregion

  //#region getQuizAfterClass
  quizAfterClass(): void {
    let url = `${this._config.apiUrl}/quiz`;
    switch (this.class) {
      case 'latest':
      default:
        this.title = 'Najnowsze quizy';
        url += '/Latest';
        break;
      case 'byTitle':
        this.title = 'Najstarsze quizy';
        url += '/Oldest';
        break;
      case 'random':
        this.title = 'Losowane quizy';
        url += '/Random';
        break;
    }
    this._qs.getQuizByClass(url).subscribe(_ => {
      this.quizzes = _;
    });
  }
  //#endregion
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { QuizService } from '../service/quiz.service';
import { MessageService } from '../../message.service';
import { Config, CONFIG } from '../../connection-string';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Quiz } from '../model/quiz';
import { QuizListComponent } from './quiz-list.component';
import { HttpClient } from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('QuizListComponent', () => {
  //#region  config
  let component: QuizListComponent;
  let fixture: ComponentFixture<QuizListComponent>;
  let quizService: QuizService;
  const config: Config = {
    apiUrl: 'http://localhost:11800/api/'
  };
  let httpMock: HttpTestingController;
  beforeEach(() => {
    const quizServiceStub = {
      getQuiz: () => ({ subscribe: () => ({ add: () => ({}) }) }),
      getQuizByClass: () => ({ subscribe: () => ({ add: () => ({}) }) })
    };
    const messageServiceStub = {};
    const configStub = { apiUrl: {} };
    const routerStub = { navigate: () => ({}) };
    const activatedRouteStub = {};
    const quizStub = { id: {} };
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [
        HttpClientTestingModule
      ],
      declarations: [QuizListComponent],
      providers: [
        { provide: QuizService, useClass: QuizService},
        { provide: MessageService, useValue: messageServiceStub },
        { provide: CONFIG, useValue: config },
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub }
      ]
    });
    fixture = TestBed.createComponent(QuizListComponent);
    component = fixture.componentInstance;
    quizService = TestBed.get(QuizService);
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    component = null;
    quizService = null;
    httpMock.verify();
  });
  //#endregion

  //#region  test quiz list
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it('service get by id tests', () => {
    const quiz: Quiz = ({
      id: 1,
      description: 'fsd',
      text: 'fdfds',
      title: 'fddsfds'
    });

    // TODO : mock http client

  });

  it('service get all quiz', () => {
    const quizs: Quiz[] = [
      {
        id: 1,
        description: '1',
        text: '1',
        title: '1'
      },
      {
        id: 2,
        description: '2',
        text: '2',
        title: '2'
      },
      {
        id: 3,
        description: '3',
        text: '3',
        title: '3'
      }
    ];

    let quizList: Quiz[] = [];

    quizService.getQuizByClass(`${config.apiUrl}/Latest`).subscribe(_ => {
      quizList = _;
    });

    const req = httpMock.expectOne(`${config.apiUrl}/Latest`);
    expect(req.request.method).toBe('GET');
    req.flush(quizs);

    expect(quizList.length).toBe(3);
    expect(quizList).toEqual(quizs);

  });

  //#endregion
});

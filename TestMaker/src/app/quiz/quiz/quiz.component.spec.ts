
import { CONFIG, Config } from 'src/app/connection-string';
import { ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { QuizService } from '../service/quiz.service';
import { MessageService } from '../../message.service';
import { QuizComponent } from './quiz.component';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Quiz } from '../model/quiz';
import { of } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/core/service/loader.service';

describe('QuizComponent', () => {
  //#region  preapare tests
  let component: QuizComponent;
  let fixture: ComponentFixture<QuizComponent>;
  let quizService: QuizService;
  let massageService: MessageService;
  let httpClient: HttpClient;
  let quiz: Quiz;
  let getIdFromUrl;
  const config: Config = {
    apiUrl: 'http://localhost:11800/api/quiz'
  };
  let loaderService: LoaderService;
  let quizComponent: QuizComponent;

  beforeEach(() => {
    const activatedRouteStub = { snapshot: { params: {id : 1} } };
    const routerStub = { navigate: {} };
    const quizServiceStub = {
      getQuizById: () => ({ subscribe: () => ({ add: () => ({}) }) }),
      deleteQuiz: () => ({ subscribe: () => ({}) })
    };
    const messageServiceStub = { success: () => ({}) };
    const loaderServiceStub = { setStart: () => ({}) };
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      imports: [HttpClientModule],
      declarations: [QuizComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerStub },
        { provide: QuizService, useValue: quizServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: CONFIG, useValue: config },
        { provide: LoaderService, useValue: loaderServiceStub }
      ]
    });
    fixture = TestBed.createComponent(QuizComponent);
    quizComponent = fixture.componentInstance;
    component = fixture.componentInstance;
    quizService = TestBed.get(QuizService);
    massageService = TestBed.get(MessageService);
    httpClient = TestBed.get(HttpClient);
    getIdFromUrl = TestBed.get(ActivatedRoute);
    loaderService = TestBed.get(LoaderService);
    quizService = TestBed.get(QuizService);
  });
  //#endregion

  //#region test
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it('can load service', () => {
    expect(quizService).toBeTruthy();
  });
  //#endregion

  //#region  test service
  it('can load message service', () => {
    expect(massageService).toBeTruthy();
  });

  it('test model class', () => {

    const excpectedQuiz: Quiz = {
      id: 1,
      description: '12',
      text: '34',
      title: '56'
    };

    quiz = {
      id: excpectedQuiz.id,
      description: excpectedQuiz.description,
      text: excpectedQuiz.text,
      title: excpectedQuiz.title
    };


    expect(quiz.id).toBe(excpectedQuiz.id);
    expect(quiz.description).toBe(excpectedQuiz.description);
    expect(quiz.text).toBe(excpectedQuiz.text);
    expect(quiz.title).toBe(excpectedQuiz.title);
  });

  it('test api url connection string', () => {
    const expectedApiUrl = config.apiUrl;
    expect(config.apiUrl).toBe(expectedApiUrl);
  });

  it('quiz test service ', () => {

    const excpectedQuiz: Quiz = {
      id: 1,
      description: '12',
      text: '34',
      title: '56'
    };


    let response: Quiz;

    spyOn(quizService, 'getQuizById').and.returnValue(of(excpectedQuiz));

    quizService.getQuizById(1).subscribe(res => {
      response = res;
    });

    expect(quizService).toBeTruthy();
    expect(response).toEqual(excpectedQuiz);

  });
  it('check get id from url ' ,  () => {

    expect(quizComponent).toBeTruthy();

    quizComponent.getQuizByPathId();

    expect(getIdFromUrl.snapshot.params['id']).toBe(1);
  });
  //#endregion
});

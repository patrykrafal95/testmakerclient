import { Component, OnInit, Input, AfterContentInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizService } from '../service/quiz.service';
import { MessageService } from '../../message.service';
import { Quiz } from '../model/quiz';
import * as $ from 'jquery';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'qui-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  //#region prop
  quiz: Quiz;
  //#endregion

  //#region constr
  constructor(
    private _activRoute: ActivatedRoute,
    private _router: Router,
    private _quizService: QuizService,
    private _ms: MessageService
  ) { }
  //#endregion

  //#region onInit
  ngOnInit() {
    this.getQuizByPathId();
    this.tabPaneChange();
  }
  //#endregion

  //#region  tab pane
  private tabPaneChange() {
    $('.navigation a').off('click').on('click', function (e) {
      e.preventDefault();

      const native = $(this);
      const id = native.attr('href').substring(native.attr('href').indexOf('#') + 1);
      $('.navigation>li>a').parent().removeClass('active');
      native.parent().hide().fadeIn().addClass('active');
      $('.tab-content').children().css({ 'display': 'none' });
      $(`.tab-content>#${id}`).css({ 'display': 'block' });
    });
  }
  //#endregion

  //#region  get quiz by id
  getQuizByPathId(): void {
    this.quiz = <Quiz>{};
    const id = this._activRoute.snapshot.params['id'];
    console.log(`odebrane id to ${id}`);
    if (id) {
      this._quizService
        .getQuizById(id)
        .subscribe(_ => {
          this.quiz = _;
        });
    } else {
      // tslint:disable-next-line:no-unused-expression
      this._router.navigate['home'];
    }
  }
  //#endregion

  //#region  onEdit
  onEdit() {
    this._router.navigate(['quiz/edit', this.quiz.id]);
  }
  //#endregion

  //#region  onBack
  onBack() {
    this._router.navigate(['home']);
  }
  //#endregion

  //#region
  onDelete(confirm: boolean) {
    if (confirm) {
      const id = +this._activRoute.snapshot.params['id'];
      if (id) {
        this._quizService.deleteQuiz(id).subscribe(_ => {
          this._ms.success('Skasowano');
          this._router.navigate(['home']);
        });
      }
    }
  }
  //#endregion

  //#region have any questions
  haveAnyQuestins() {
    const id = + this._activRoute.snapshot.params['id'];
    if (id) {
      this._quizService.quizHaveAnyQuestions(id).subscribe(_ => {
        if (!_) {
          this._ms.warrning('Test nie zawiera pytań');
        } else {
          this._ms.success('Test zawiera pytania, zostaniesz przekierowany do testu');
        }
      });
    }
  }
  //#endregion
}

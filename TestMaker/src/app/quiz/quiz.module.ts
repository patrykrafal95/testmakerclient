import { QuestionListComponent } from './../question/question-list/question-list.component';
import { NgModule } from '@angular/core';
import { QuizListComponent } from './quiz-list/quiz-list.component';
import { QuizComponent } from './quiz/quiz.component';
import { SharedModule } from '../sheard/sheard.module';
import { QuizEditComponent } from './quiz-edit/quiz-edit.component';
import { RouterModule } from '@angular/router';
import { QuizLoadDirective } from './directives/quiz-load.directive';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [QuizListComponent, QuizComponent, QuizEditComponent, QuizLoadDirective],
  exports: [
    QuizListComponent,
    QuizComponent,
    QuizEditComponent
  ]
})
export class QuizModule { }

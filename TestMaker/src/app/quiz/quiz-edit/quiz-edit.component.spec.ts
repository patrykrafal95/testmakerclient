import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { QuizService } from '../service/quiz.service';
import { MessageService } from '../../message.service';
import { QuizEditComponent } from './quiz-edit.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoaderService } from 'src/app/core/service/loader.service';

describe('QuizEditComponent', () => {
  let component: QuizEditComponent;
  let fixture: ComponentFixture<QuizEditComponent>;
  let getIdFromUrl;
  let quizService: QuizService;
  let loaderService: LoaderService;
  let formGroup: FormGroup;
  let formBulider: FormBuilder;
  beforeEach(() => {
    const activatedRouteStub = { snapshot: { params: {id : 1} } };
    const routerStub = { navigate: () => ({}) };
    const httpClientStub = {};
    const quizServiceStub = {
      getQuizById: () => ({ subscribe: () => ({ add: () => ({}) }) }),
      editQuiz: () => ({ subscribe: () => ({ add: () => ({}) }) }),
      addQuiz: () => ({ subscribe: () => ({ add: () => ({}) }) })
    };
    const messageServiceStub = { success: () => ({}) };
    const quizStub = {};
    const loaderSeriviceStub = { setStart: () => ({}) };
    const formBuilderStub = { group: ({ }) => ({}) };

    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [QuizEditComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Router, useValue: routerStub },
        { provide: HttpClient, useValue: httpClientStub },
        { provide: QuizService, useValue: quizServiceStub },
        { provide: MessageService, useValue: messageServiceStub },
        { provide: LoaderService, useValue: loaderSeriviceStub},
        { provide: FormBuilder, useValue: formBuilderStub },
        { provide: FormGroup, useValue: formGroup }
      ]
    });
    fixture = TestBed.createComponent(QuizEditComponent);
    component = fixture.componentInstance;
    getIdFromUrl = TestBed.get(ActivatedRoute);
    quizService = TestBed.get(QuizService);
    loaderService = TestBed.get(LoaderService);
    formGroup = TestBed.get(FormGroup);
    formBulider = TestBed.get(FormBuilder);

    formGroup = formBulider.group({
      title: ['', Validators.required],
      description: '',
      text: ''
    });

  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  describe('onSubmit', () => {
    it('makes expected calls', () => {
      const routerStub: Router = fixture.debugElement.injector.get(Router);
      const quizServiceStub: QuizService = fixture.debugElement.injector.get(
        QuizService
      );
      const messageServiceStub: MessageService = fixture.debugElement.injector.get(
        MessageService
      );

    });


  it('quiz edit is true', () => {
    const id = getIdFromUrl.snapshot.params['id'];


    component.quizEdit();
    expect(component).toBeTruthy();
    expect(id).toBe(1);
    expect(component.editMode).toBe(true);

  });

  });
  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      spyOn(component, 'quizEdit');
      component.ngOnInit();
      expect(component.quizEdit).toHaveBeenCalled();
    });
  });

});

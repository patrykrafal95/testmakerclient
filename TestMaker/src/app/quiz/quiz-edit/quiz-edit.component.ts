import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { QuizService } from '../service/quiz.service';
import { MessageService } from '../../message.service';
import { Quiz } from '../model/quiz';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-quiz-edit',
  templateUrl: './quiz-edit.component.html',
  styleUrls: ['./quiz-edit.component.scss']
})
export class QuizEditComponent implements OnInit {
  title: string;
  quiz: Quiz;
  editMode: boolean;
  form: FormGroup;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _http: HttpClient,
    private _quizService: QuizService,
    private _ms: MessageService,
    private _fb: FormBuilder
  ) {}

  ngOnInit() {
    this.quizEdit();
  }

  //#region edit quiz
  quizEdit() {
    this.quiz = <Quiz>{};
    this.createForm();
    const id = +this._activatedRoute.snapshot.params['id'];
    if (id) {
      this.editMode = true;
      this._quizService
        .getQuizById(id)
        .subscribe(_ => {
          this.quiz = _;
          this.title = `Edycja ${this.quiz.title}`;
          this.updateForm();
        });
    } else {
      this.editMode = false;
      this.title = 'Utwórz nowy quiz';
    }
  }
  //#endregion

  //#region onSubmit quiz
  onSubmit() {
    const bufQuiz = <Quiz>{};
    bufQuiz.title = this.form.value.title;
    bufQuiz.description = this.form.value.description;
    bufQuiz.text = this.form.value.text;


    if (this.editMode) {
      bufQuiz.id = this.quiz.id;
      this._quizService.editQuiz(bufQuiz).subscribe(_ => {
        console.log(`Quiz aktualizwoanyu`);
        this._ms.success('Zaktualizowano pomyślnie');
        this._router.navigate(['home']);
      });
    } else {
      console.log('dodawanie');
      this._quizService.addQuiz(bufQuiz).subscribe(_ => {
        this._ms.success('Dodano pomyślnie');
        this._router.navigate(['home']);
      });
    }
  }
  //#endregion

  //#region back
  onBack() {
    this._router.navigate(['home']);
  }
  //#endregion

  //#region  create form to validation
  createForm() {
    this.form = this._fb.group({
      title: ['', Validators.required],
      description: '',
      text: ''
    });
  }
  //#endregion

  //#region update form
  updateForm() {
    this.form.setValue({
      title: this.quiz.title,
      description: this.quiz.description || '',
      text: this.quiz.text || ''
    });
  }
  //#endregion

  //#region get form controll
  getFormControl(name: string) {
    return this.form.get(name);
  }

  isValid(name: string) {
    const e = this.getFormControl(name);
    return e && e.valid;
  }

  hasError(name: string) {
    const e = this.getFormControl(name);
    return e && (e.dirty || e.touched) && !e.valid;
  }
  //#endregion
}

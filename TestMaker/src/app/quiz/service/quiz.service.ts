import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONFIG, Config } from 'src/app/connection-string';
import { Quiz } from '../model/quiz';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  //#region constructor
  constructor(private _hs: HttpClient,
    @Inject(CONFIG) private _conn: Config
  ) {

  }
  //#endregion

  //#region get quiz
  getQuiz() {
    return this._hs.get<Quiz[]>(`${this._conn.apiUrl}/quiz/Latest`);
  }
  //#endregion

  getQuizByClass(url: string) {
    console.log(url);
    return this._hs.get<Quiz[]>(`${url}`);
  }

  getQuizById(id: number) {
    return this._hs.get<Quiz>(`${this._conn.apiUrl}/quiz/GetQuiz/${id}`);
  }

  editQuiz(quiz: Quiz) {
    return this._hs.post(`${this._conn.apiUrl}/quiz/EditQuiz`, quiz);
  }

  deleteQuiz(id: number) {
    return this._hs.get<Quiz>(`${this._conn.apiUrl}/quiz/RemoveQuiz/${id}`);
  }

  addQuiz(quiz: Quiz) {
    return this._hs.post<Quiz>(`${this._conn.apiUrl}/quiz/AddQuiz`, quiz);
  }

  quizHaveAnyQuestions(id: number) {
    return this._hs.get<boolean>(`${this._conn.apiUrl}/quiz/QuizHaveQuesion/${id}`);
  }

}
